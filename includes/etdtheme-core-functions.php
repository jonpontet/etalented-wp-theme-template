<?php
/**
 * Core Functions
 *
 * General core functions available on both the front-end and admin.
 */

defined( 'ABSPATH' ) || exit;

/**
 * Define a constant if it is not already defined.
 */
function etdtheme_maybe_define_constant( $name, $value ) {
	if ( ! defined( $name ) ) {
		define( $name, $value );
	}
}